#include "th_monitering.h"

Th_monitering::Th_monitering(QVector<machine_statue_data> datalist)
{
    this->datalist = datalist;
    QString msdb_name = QString("MS_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    while(true){
        if(!ms_mesdb.contains(msdb_name)){
            ms_mesdb = QSqlDatabase::addDatabase("QODBC",msdb_name);
            QString serverinfo = "DRIVER={SQL Server};Server=10.20.10.221;Database=WOSFDB;Uid=mesview_fab2;Port=1433;Pwd=mesview1705";
            ms_mesdb.setDatabaseName(serverinfo);
            if(!ms_mesdb.open()){
                qDebug()<<"fasle";
                qDebug()<<ms_mesdb.lastError().text();
            }else {
                qDebug()<<"open";
            }
            break;
        }else {
            msdb_name.append("S");
        }
    }
    while(true){
        QString mydb_name = QString("MY_MESDB_OI_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
        if(!my_mesdb.contains(mydb_name)){
            my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
            my_mesdb.setHostName("fabsv.wisol.co.kr");
            my_mesdb.setPort(3306);
            my_mesdb.setUserName(DBID);
            my_mesdb.setPassword(DBPW);
            my_mesdb.setDatabaseName("FAB");
            if(!my_mesdb.open()){
                qDebug()<<"fasle";
                qDebug()<<my_mesdb.lastError().text();
            }else {
                qDebug()<<"open";
            }
            break;
        }else {
            mydb_name.append("S");
        }
    }

}
Th_monitering::Th_monitering(QVector<machine_statue_data> datalist,QMutex *mu)
{
    this->datalist = datalist;
    this->mu = mu;
    QString msdb_name = QString("MS_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
    while(true){
        if(!ms_mesdb.contains(msdb_name)){
            ms_mesdb = QSqlDatabase::addDatabase("QODBC",msdb_name);
            QString serverinfo = "DRIVER={SQL Server};Server=10.20.10.221;Database=WOSFDB;Uid=mesview_fab2;Port=1433;Pwd=mesview1705";
            ms_mesdb.setDatabaseName(serverinfo);
            if(!ms_mesdb.open()){
                qDebug()<<"fasle";
                qDebug()<<ms_mesdb.lastError().text();
            }else {
                qDebug()<<"open";
            }
            break;
        }else {
            msdb_name.append("S");
        }
    }
    while(true){
        QString mydb_name = QString("MY_MESDB_OI_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
        if(!my_mesdb.contains(mydb_name)){
            my_mesdb = QSqlDatabase::addDatabase("QMYSQL",mydb_name);
            my_mesdb.setHostName("fabsv.wisol.co.kr");
            my_mesdb.setPort(3306);
            my_mesdb.setUserName(DBID);
            my_mesdb.setPassword(DBPW);
            my_mesdb.setDatabaseName("FAB");
            if(!my_mesdb.open()){
                qDebug()<<"fasle";
                qDebug()<<my_mesdb.lastError().text();
            }else {
                qDebug()<<"open";
            }
            break;
        }else {
            mydb_name.append("S");
        }
    }
}

void Th_monitering::re_db_connect()
{
    ms_mesdb.close();
    QString msdb_name = QString("MS_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmsszzz"));
    mu->lock();
        if(!ms_mesdb.contains(msdb_name)){
            ms_mesdb = QSqlDatabase::addDatabase("QODBC",msdb_name);
            QString serverinfo = "DRIVER={SQL Server};Server=10.20.10.221;Database=WOSFDB;Uid=mesview_fab2;Port=1433;Pwd=mesview1705";
            ms_mesdb.setDatabaseName(serverinfo);
            ms_mesdb.setConnectOptions();
            if(!ms_mesdb.open()){
                qDebug()<<"fasle";
                qDebug()<<ms_mesdb.lastError().text();
            }else {
                qDebug()<<"open";
            }

        }else {
            msdb_name.append("S");
        }
    mu->unlock();

}

void Th_monitering::run()
{
    bool USCHDOWN3_flag = false;
    while(!isInterruptionRequested()){
        QSqlQuery query(ms_mesdb);
         int i =0;
        QString append_txt;
        QVector<machine_statue_data> current_datalist;
        for(int i=0;i<datalist.count();i++){
            if(i==datalist.count()-1){
                append_txt = append_txt.append(QString("'%1'").arg(datalist.at(i).getMachine_code()));
            }else {
                append_txt = append_txt.append(QString("'%1',").arg(datalist.at(i).getMachine_code()));
            }
        }
        query.exec(QString("select EQUIPMENT_ID,EQUIPMENT_NAME,LAST_EVENT_ID from V_NM_EQUIPMENT (NOLOCK) where EQUIPMENT_ID IN(%1) AND DELETE_FLAG = 'N'").arg(append_txt));
        if((query.lastError().type() != QSqlError::NoError ) || !ms_mesdb.isOpen() ){
            qDebug()<<query.lastError().type();
            qDebug()<<query.lastError().text();
            qDebug()<<query.lastQuery();
            emit send_log_text(QDateTime::currentDateTime().toString("[yyyyMMdd hh:mm:ss] ")+query.lastError().text());
            QThread::sleep(15);
            emit send_log_text(QDateTime::currentDateTime().toString("[yyyyMMdd hh:mm:ss] ")+"reconnet try");
            emit send_test_query(QString("reconnect"));
            re_db_connect();
            continue;
        }
        while(query.next()){
            i++;
            machine_statue_data temp_data;
            machine_statue_data temp_data_view;
            temp_data.setMachine_code(query.value("EQUIPMENT_ID").toString());
            temp_data_view.setMachine_code(query.value("EQUIPMENT_ID").toString());
            QString event_data= query.value("LAST_EVENT_ID").toString();
            restart:
            if(event_data=="RUN"){
                temp_data_view.setCurrent_event(tr("RUN"));
                temp_data_view.setColor(QColor(0,170,0));
                temp_data.setCurrent_event(tr("RUN"));
                temp_data.setColor(QColor("#80ff80"));
            }else if(event_data=="CVD_SiN"){
                temp_data_view.setCurrent_event(tr("RUN"));
                temp_data_view.setColor(QColor(0,170,0));
                temp_data.setCurrent_event(tr("CVD_SiN"));
                temp_data.setColor(QColor("#80ff80"));
            }else if(event_data=="CVD_ONO"){
                temp_data_view.setCurrent_event(tr("RUN"));
                temp_data_view.setColor(QColor(0,170,0));
                temp_data.setCurrent_event(tr("CVD_ONO"));
                temp_data.setColor(QColor("#80ff80"));
            }else if(event_data=="ENGR1"){
                temp_data_view.setCurrent_event(tr("ENGR1"));
                temp_data_view.setColor(QColor(85,0,255));
                temp_data.setCurrent_event(tr("ENGR1"));
                temp_data.setColor(QColor("#8080ff"));
            }else if(event_data=="SCHDOWN1"){
                temp_data_view.setCurrent_event(tr("SCHDOWN1"));
                temp_data_view.setColor(QColor(255,0,255));
                temp_data.setCurrent_event(tr("SCHDOWN1"));
                temp_data.setColor(QColor("#ff80ff"));
            }else if(event_data=="USCHDOWN3"){
                temp_data.setCurrent_event(tr("USCHDOWN3"));
                temp_data_view.setCurrent_event(tr("USCHDOWN3"));
                if(!USCHDOWN3_flag){
                    temp_data_view.setColor(QColor("#ff1f21"));
                    temp_data.setColor(QColor("#ff1f21"));
                }else {
                    temp_data_view.setColor(QColor("#FF9090"));
                    temp_data.setColor(QColor("#FF9090"));
                }
            }else if(event_data=="WAIT"){
                temp_data_view.setCurrent_event(tr("WAIT"));
                temp_data_view.setColor(QColor(93,93,0));
                temp_data.setCurrent_event(tr("WAIT"));
                temp_data.setColor(QColor("#ffff80"));
            }else if(event_data=="SCHDOWN2"){
                temp_data_view.setCurrent_event(tr("SCHDOWN2"));
                temp_data_view.setColor(QColor(93,93,0));
                temp_data.setCurrent_event(tr("SCHDOWN2"));
                temp_data.setColor(QColor("#ffff80"));
            }else if(event_data=="USCHDOWN4"){
                temp_data_view.setCurrent_event(tr("USCHDOWN4"));
                temp_data_view.setColor(QColor(93,93,0));
                temp_data.setCurrent_event(tr("USCHDOWN4"));
                temp_data.setColor(QColor("#ffff80"));
            }else if(event_data=="USCHDOWN1"){
                temp_data_view.setCurrent_event(tr("USCHDOWN1"));
                temp_data_view.setColor(QColor(93,93,0));
                temp_data.setCurrent_event(tr("USCHDOWN1"));
                temp_data.setColor(QColor("#ffff80"));
            }else if(event_data=="USCHDOWN2"){
                temp_data_view.setCurrent_event(tr("USCHDOWN2"));
                temp_data_view.setColor(QColor(93,93,0));
                temp_data.setCurrent_event(tr("USCHDOWN2"));
                temp_data.setColor(QColor("#ffff80"));
            }else if(event_data=="NONSCHED"){
                temp_data_view.setCurrent_event(tr("NONSCHED"));
                temp_data_view.setColor(QColor(93,93,0));
                temp_data.setCurrent_event(tr("NONSCHED"));
                temp_data.setColor(QColor("#ffff80"));
            }else if(event_data=="TX_START"){
                QSqlQuery myquery(my_mesdb);
                myquery.exec(QString("select before_statue from OI_system_machine_table_ver2 where machine_code = '%1' ").arg(temp_data_view.getMachine_code()));
                if(myquery.next()){
                    event_data = myquery.value("before_statue").toString();

                    goto restart;
                }
            }else if(event_data=="TX_END"){
                QSqlQuery myquery(my_mesdb);
                myquery.exec(QString("select before_statue from OI_system_machine_table_ver2 where machine_code = '%1' ").arg(temp_data_view.getMachine_code()));
                if(myquery.next()){
                    event_data = myquery.value("before_statue").toString();

                    goto restart;
                }
            }else if(event_data=="CONTROL_MODE"){
                QSqlQuery myquery(my_mesdb);
                myquery.exec(QString("select before_statue from OI_system_machine_table_ver2 where machine_code = '%1' ").arg(temp_data_view.getMachine_code()));
                if(myquery.next()){
                    event_data = myquery.value("before_statue").toString();

                    goto restart;
                }
            }else {
                emit send_log_text(QString("event_data = %1 ").arg(event_data));
                temp_data_view.setCurrent_event(tr("dontknow"));
                temp_data_view.setColor(QColor("#ffffff"));
                temp_data.setCurrent_event(tr("dontknow"));
                temp_data.setColor(QColor("#ffffff"));
            }
            emit send_listdata_view(temp_data_view);
            emit send_listdata(temp_data);
            current_datalist.append(temp_data);
        }

        if(!USCHDOWN3_flag){
            USCHDOWN3_flag=true;
        }else {
            USCHDOWN3_flag = false;
        }
//        if (i == 0){
//            QString msdb_name = QString("MS_MESDB_%1").arg(QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss"));
//            while(true){
//                ms_mesdb.close();
//                msdb_name.append("S");
//                if(!ms_mesdb.contains(msdb_name)){
//                    ms_mesdb = QSqlDatabase::addDatabase("QODBC",msdb_name);
//                    QString serverinfo = "DRIVER={SQL Server};Server=10.20.10.221;Database=MESDB;Uid=mesview_fab2;Port=1433;Pwd=mesview1705";
//                    ms_mesdb.setDatabaseName(serverinfo);

//                        if(!ms_mesdb.open()){
//                            qDebug()<<"fasle";
//                            qDebug()<<ms_mesdb.lastError().text();
//                        }else {
//                            qDebug()<<"open";
//                        }

//                    break;
//                }else {
//                    msdb_name.append("S");
//                }
//            }
//        }
        emit send_test_query(QString("%1").arg(i));
        if(i==0){
            emit send_log_text(QDateTime::currentDateTime().toString("[yyyyMMdd hh:mm:ss] ")+" i = 0 AND append text  = "+append_txt);
            emit send_log_text(QDateTime::currentDateTime().toString("[yyyyMMdd hh:mm:ss] ")+"reconnet try");
            re_db_connect();
        }
        QThread::sleep(2);
    }
    qDebug()<<"delete";
    emit send_test_query(QString("delete"));

}
