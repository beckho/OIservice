#include "b_label.h"

B_Label::B_Label(QString str): QLabel(str)
{

}
B_Label::B_Label(QString str, QString machine_name, QString machine_code, QString process, QSqlDatabase my_mes_db): QLabel(str)
{
    this->machine_name = machine_name;
    this->machine_code = machine_code;
    this->process = process;
    this->my_mes_db = my_mes_db;
    if(str.indexOf("CVD") >=0){
        menu.addAction(tr("CVD_SiN"),this,SLOT(SINMODE1()));
        menu.addAction(tr("CVD_ONO"),this,SLOT(ONOMODE1()));
    }
    menu.addAction(tr("RUN"),this,SLOT(RUN_slot()));
    menu.addAction(tr("ENGR1"),this,SLOT(ENGR1_slot()));
    menu.addAction(tr("SCHDOWN1"),this,SLOT(SCHDOWN1_slot()));
    menu.addAction(tr("USCHDOWN3"),this,SLOT(USCHDOWN3_slot()));
    menu.addAction(tr("WAIT"),this,SLOT(WAIT_slot()));
    menu.addAction(tr("SCHDOWN2"),this,SLOT(SCHDOWN2_slot()));

    QSqlQuery query(my_mes_db);
    query.exec("select machine_name,machine_code from Thin_film_spec_managerment");
    while(query.next()){
        if(query.value("machine_code").toString() == machine_code){
            menu.addAction(tr("Clean_RUN"),this,SLOT(RUN_Cleaning_slot()));
        }
    }
    flag_munu=true;

    this->setAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
    QFont font = this->font();
    font.setBold(true);
    this->setFont(font);
}

QString B_Label::getProcess() const
{
    return process;
}

void B_Label::setProcess(const QString &value)
{
    process = value;
}


QString B_Label::getMachine_name() const
{
    return machine_name;
}

void B_Label::setMachine_name(const QString &value)
{
    machine_name = value;
}

QString B_Label::getMachine_code() const
{
    return machine_code;
}

void B_Label::setMachine_code(const QString &value)
{
    machine_code = value;
}

void B_Label::mousePressEvent(QMouseEvent *ev)
{
    QLabel::mousePressEvent(ev);
    if(ev->button()==Qt::RightButton){
        if(flag_munu){
            menu.exec(QCursor::pos());
        }
    }
}

void B_Label::RUN_slot()
{
    login_form *from = new login_form("RUN",machine_code,machine_name,process);
    from->show();
}

void B_Label::ENGR1_slot()
{
    login_form *from = new login_form("ENGR1",machine_code,machine_name,process);
    from->show();
}

void B_Label::SCHDOWN1_slot()
{
    login_form *from = new login_form("SCHDOWN1",machine_code,machine_name,process);
    from->show();
}

void B_Label::SCHDOWN2_slot()
{
    login_form *from = new login_form("SCHDOWN2",machine_code,machine_name,process);
    from->show();
}

void B_Label::USCHDOWN3_slot()
{
    login_form *from = new login_form("USCHDOWN3",machine_code,machine_name,process);
    from->show();
}

void B_Label::WAIT_slot()
{
    login_form *from = new login_form("WAIT",machine_code,machine_name,process);
    from->show();
}

void B_Label::RUN_Cleaning_slot()
{
    login_form *from = new login_form("Clean_RUN",machine_code,machine_name,process);
    from->show();
}

void B_Label::SINMODE1()
{
    login_form *from = new login_form("CVD_SiN",machine_code,machine_name,process);
    from->show();
}

void B_Label::ONOMODE1()
{
    login_form *from = new login_form("CVD_ONO",machine_code,machine_name,process);
    from->show();
}
