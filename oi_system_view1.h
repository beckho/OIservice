#ifndef OI_SYSTEM_VIEW1_H
#define OI_SYSTEM_VIEW1_H

#include <QWidget>
#include <QStandardItem>
#include <QStandardItemModel>
#include <machine_statue_data.h>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <bstandarditem.h>
#include <QDateTime>
#include <QTimer>
#define DBID "EIS"
#define DBPW "wisolfab!"
namespace Ui {
class oi_system_view1;
}

class oi_system_view1 : public QWidget
{
    Q_OBJECT

public:
    explicit oi_system_view1(QSqlDatabase my_mesdb,QWidget *parent = 0);
    explicit oi_system_view1(QHash<QString,machine_statue_data> *machine_statue_map,QWidget *parent = 0);
    QSqlDatabase my_mesdb;
    QStandardItemModel *model1;
    QVector<BStandardItem *>itemlist;
    QVector<BStandardItem *>probe_itemlist;
    BStandardItem *findforitem(QVector<BStandardItem *> &itemlist,QString machine_code);
    QTimer summary_loop;


    ~oi_system_view1();
public slots:
    void T1_slot(machine_statue_data data);
    void T2_slot(machine_statue_data data);
    void summary_loop_timeout();


private:
    void closeEvent(QCloseEvent *event);
    Ui::oi_system_view1 *ui;
};

#endif // OI_SYSTEM_VIEW1_H
