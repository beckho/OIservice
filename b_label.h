#ifndef B_LABEL_H
#define B_LABEL_H

#include <QObject>
#include <QLabel>
#include <QMouseEvent>
#include <QMenu>
#include <QDebug>
#include <login_form.h>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
class B_Label : public QLabel
{
    Q_OBJECT
public:
    explicit B_Label(QString str);
    explicit B_Label(QString str, QString machine_name, QString machine_code, QString process,QSqlDatabase my_mes_db);
    explicit B_Label(QString str, QString machine_name, QString machine_code, QString process,QColor color);
    QString machine_code;
    QString machine_name;
    QString process;
    bool flag_munu;
    QMenu menu;
    QSqlDatabase my_mes_db;
    QString getMachine_code() const;
    void setMachine_code(const QString &value);

    QString getMachine_name() const;
    void setMachine_name(const QString &value);


    QString getProcess() const;
    void setProcess(const QString &value);

private:
    void mousePressEvent(QMouseEvent *ev);

signals:

public slots:
    void RUN_slot();
    void ENGR1_slot();
    void SCHDOWN1_slot();
    void SCHDOWN2_slot();
    void USCHDOWN3_slot();
    void WAIT_slot();
    void RUN_Cleaning_slot();
    void SINMODE1();
    void ONOMODE1();
};

#endif // B_LABEL_H
