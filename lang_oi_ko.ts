<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR">
<context>
    <name>B_Label</name>
    <message>
        <location filename="b_label.cpp" line="14"/>
        <source>CVD_SiN</source>
        <oldsource>SINMODE1</oldsource>
        <translation type="unfinished">SINMODE1</translation>
    </message>
    <message>
        <location filename="b_label.cpp" line="15"/>
        <source>CVD_ONO</source>
        <oldsource>ONOMODE1</oldsource>
        <translation type="unfinished">ONOMODE1</translation>
    </message>
    <message>
        <location filename="b_label.cpp" line="17"/>
        <source>RUN</source>
        <translation>정상가동</translation>
    </message>
    <message>
        <location filename="b_label.cpp" line="18"/>
        <source>ENGR1</source>
        <translation>공정,설비 테스트</translation>
    </message>
    <message>
        <location filename="b_label.cpp" line="19"/>
        <source>SCHDOWN1</source>
        <translation>예방정비</translation>
    </message>
    <message>
        <location filename="b_label.cpp" line="20"/>
        <source>USCHDOWN3</source>
        <translation>설비고장</translation>
    </message>
    <message>
        <location filename="b_label.cpp" line="21"/>
        <source>WAIT</source>
        <translation>대기</translation>
    </message>
    <message>
        <location filename="b_label.cpp" line="22"/>
        <source>SCHDOWN2</source>
        <translation>기종변경</translation>
    </message>
    <message>
        <location filename="b_label.cpp" line="28"/>
        <source>Clean_RUN</source>
        <oldsource>Clean_SCHDOWN1</oldsource>
        <translation>클리닝 후 정상가동</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="31"/>
        <source>모니터링</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="853"/>
        <location filename="mainwindow.ui" line="929"/>
        <location filename="mainwindow.ui" line="954"/>
        <location filename="mainwindow.ui" line="996"/>
        <location filename="mainwindow.ui" line="1024"/>
        <source>설비코드</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="969"/>
        <location filename="mainwindow.ui" line="1102"/>
        <location filename="mainwindow.ui" line="1293"/>
        <location filename="mainwindow.ui" line="1620"/>
        <location filename="mainwindow.ui" line="1819"/>
        <source>프로브</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="868"/>
        <location filename="mainwindow.ui" line="1097"/>
        <location filename="mainwindow.ui" line="1288"/>
        <location filename="mainwindow.ui" line="1603"/>
        <location filename="mainwindow.ui" line="1840"/>
        <location filename="mainwindow.ui" line="1928"/>
        <source>에칭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="885"/>
        <location filename="mainwindow.ui" line="1092"/>
        <location filename="mainwindow.ui" line="1283"/>
        <location filename="mainwindow.ui" line="1586"/>
        <location filename="mainwindow.ui" line="1771"/>
        <location filename="mainwindow.ui" line="1921"/>
        <source>노광</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="902"/>
        <location filename="mainwindow.ui" line="1087"/>
        <location filename="mainwindow.ui" line="1278"/>
        <location filename="mainwindow.ui" line="1569"/>
        <location filename="mainwindow.ui" line="1785"/>
        <location filename="mainwindow.ui" line="1914"/>
        <source>성막</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="620"/>
        <source>공정,설비
 테스트</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="178"/>
        <source>정상
가동</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="473"/>
        <source>예방
정비</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="325"/>
        <source>설비
고장</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="767"/>
        <location filename="mainwindow.ui" line="1329"/>
        <location filename="mainwindow.ui" line="1432"/>
        <location filename="mainwindow.ui" line="1660"/>
        <source>대기</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="774"/>
        <source>관리자모드</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="798"/>
        <source>추가</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="805"/>
        <source>수정</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="812"/>
        <source>제거</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1037"/>
        <source>기록</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1074"/>
        <location filename="mainwindow.ui" line="1377"/>
        <source>공정</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1082"/>
        <location filename="mainwindow.ui" line="1273"/>
        <source>전체</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1110"/>
        <source>설비</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1127"/>
        <location filename="mainwindow.ui" line="1150"/>
        <source>yyyy-MM-dd hh:mm:ss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1143"/>
        <location filename="mainwindow.ui" line="1213"/>
        <location filename="mainwindow.ui" line="1251"/>
        <source>~</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1160"/>
        <source>검색</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1183"/>
        <source>레포트</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1230"/>
        <location filename="mainwindow.ui" line="1244"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1237"/>
        <source>가동률 계산</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1258"/>
        <source>일별휴지시간 설정 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1265"/>
        <source>공정 선택 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1301"/>
        <source>정렬 데이터 선택 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1314"/>
        <location filename="mainwindow.ui" line="1645"/>
        <source>공정,설비테스트</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1359"/>
        <source>이름순</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1369"/>
        <source>전체보기</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1382"/>
        <source>설비이름</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1387"/>
        <source>정지로스</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1407"/>
        <source>공설 설비 테스트 시간</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1417"/>
        <source>예방정비 시간</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1447"/>
        <source>순간정지 시간</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1457"/>
        <source>자재품절 시간</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1467"/>
        <source>기종변경 시간</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1477"/>
        <source>품질문제 시간</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1487"/>
        <source>휴지 시간</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1501"/>
        <source>그래프</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1632"/>
        <source>정렬 순서</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1709"/>
        <source>Worst10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1742"/>
        <source>기타 설정</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1750"/>
        <location filename="mainwindow.ui" line="1757"/>
        <location filename="mainwindow.ui" line="1764"/>
        <location filename="mainwindow.ui" line="1778"/>
        <location filename="mainwindow.ui" line="1792"/>
        <location filename="mainwindow.ui" line="1812"/>
        <location filename="mainwindow.ui" line="1833"/>
        <location filename="mainwindow.ui" line="1847"/>
        <location filename="mainwindow.ui" line="1861"/>
        <location filename="mainwindow.ui" line="1868"/>
        <source>99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1805"/>
        <source>row 사이즈</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1826"/>
        <source>글자 크기</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1854"/>
        <location filename="mainwindow.ui" line="1935"/>
        <source>프로브1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1879"/>
        <location filename="mainwindow.ui" line="1886"/>
        <location filename="mainwindow.ui" line="1893"/>
        <location filename="mainwindow.ui" line="1900"/>
        <location filename="mainwindow.ui" line="1907"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1942"/>
        <source>프로브2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2009"/>
        <source>메뉴</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2026"/>
        <source>전용뷰어</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1309"/>
        <location filename="mainwindow.ui" line="1392"/>
        <location filename="mainwindow.ui" line="1640"/>
        <source>정상가동</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1402"/>
        <source>공정설비테스트</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1319"/>
        <location filename="mainwindow.ui" line="1412"/>
        <location filename="mainwindow.ui" line="1650"/>
        <source>예방정비</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1324"/>
        <location filename="mainwindow.ui" line="1422"/>
        <location filename="mainwindow.ui" line="1655"/>
        <source>설비고장</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1334"/>
        <location filename="mainwindow.ui" line="1442"/>
        <location filename="mainwindow.ui" line="1665"/>
        <source>순간정지</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1339"/>
        <location filename="mainwindow.ui" line="1452"/>
        <location filename="mainwindow.ui" line="1670"/>
        <source>자재품절</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1344"/>
        <location filename="mainwindow.ui" line="1462"/>
        <location filename="mainwindow.ui" line="1675"/>
        <source>기종변경</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1349"/>
        <location filename="mainwindow.ui" line="1472"/>
        <location filename="mainwindow.ui" line="1680"/>
        <source>품질문제</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1354"/>
        <location filename="mainwindow.ui" line="1482"/>
        <location filename="mainwindow.ui" line="1685"/>
        <source>휴지</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1492"/>
        <source>정지로스(%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1549"/>
        <source>조회</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="843"/>
        <location filename="mainwindow.ui" line="919"/>
        <location filename="mainwindow.ui" line="944"/>
        <location filename="mainwindow.ui" line="986"/>
        <location filename="mainwindow.ui" line="1014"/>
        <source>설비명</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="848"/>
        <location filename="mainwindow.ui" line="924"/>
        <location filename="mainwindow.ui" line="949"/>
        <location filename="mainwindow.ui" line="991"/>
        <location filename="mainwindow.ui" line="1019"/>
        <source>설비상태</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="148"/>
        <location filename="mainwindow.cpp" line="480"/>
        <location filename="mainwindow.cpp" line="481"/>
        <location filename="mainwindow.cpp" line="539"/>
        <location filename="mainwindow.cpp" line="1033"/>
        <location filename="mainwindow.cpp" line="1034"/>
        <location filename="mainwindow.cpp" line="1096"/>
        <location filename="mainwindow.cpp" line="1102"/>
        <location filename="mainwindow.cpp" line="1188"/>
        <location filename="mainwindow.cpp" line="1193"/>
        <location filename="mainwindow.cpp" line="1294"/>
        <location filename="mainwindow.cpp" line="1516"/>
        <location filename="mainwindow.cpp" line="1522"/>
        <location filename="mainwindow.cpp" line="1588"/>
        <location filename="mainwindow.cpp" line="1593"/>
        <location filename="mainwindow.cpp" line="1702"/>
        <source>deposition</source>
        <translation>성막</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="149"/>
        <location filename="mainwindow.cpp" line="488"/>
        <location filename="mainwindow.cpp" line="489"/>
        <location filename="mainwindow.cpp" line="540"/>
        <location filename="mainwindow.cpp" line="1035"/>
        <location filename="mainwindow.cpp" line="1036"/>
        <location filename="mainwindow.cpp" line="1103"/>
        <location filename="mainwindow.cpp" line="1109"/>
        <location filename="mainwindow.cpp" line="1194"/>
        <location filename="mainwindow.cpp" line="1199"/>
        <location filename="mainwindow.cpp" line="1299"/>
        <location filename="mainwindow.cpp" line="1523"/>
        <location filename="mainwindow.cpp" line="1529"/>
        <location filename="mainwindow.cpp" line="1594"/>
        <location filename="mainwindow.cpp" line="1599"/>
        <location filename="mainwindow.cpp" line="1708"/>
        <source>light</source>
        <translation>노광</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="150"/>
        <location filename="mainwindow.cpp" line="496"/>
        <location filename="mainwindow.cpp" line="497"/>
        <location filename="mainwindow.cpp" line="541"/>
        <location filename="mainwindow.cpp" line="1037"/>
        <location filename="mainwindow.cpp" line="1038"/>
        <location filename="mainwindow.cpp" line="1110"/>
        <location filename="mainwindow.cpp" line="1116"/>
        <location filename="mainwindow.cpp" line="1200"/>
        <location filename="mainwindow.cpp" line="1205"/>
        <location filename="mainwindow.cpp" line="1304"/>
        <location filename="mainwindow.cpp" line="1530"/>
        <location filename="mainwindow.cpp" line="1536"/>
        <location filename="mainwindow.cpp" line="1600"/>
        <location filename="mainwindow.cpp" line="1605"/>
        <location filename="mainwindow.cpp" line="1714"/>
        <source>eatching</source>
        <translation>에칭</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="151"/>
        <location filename="mainwindow.cpp" line="504"/>
        <location filename="mainwindow.cpp" line="505"/>
        <location filename="mainwindow.cpp" line="512"/>
        <location filename="mainwindow.cpp" line="542"/>
        <location filename="mainwindow.cpp" line="1039"/>
        <location filename="mainwindow.cpp" line="1040"/>
        <location filename="mainwindow.cpp" line="1117"/>
        <location filename="mainwindow.cpp" line="1123"/>
        <location filename="mainwindow.cpp" line="1206"/>
        <location filename="mainwindow.cpp" line="1211"/>
        <location filename="mainwindow.cpp" line="1310"/>
        <location filename="mainwindow.cpp" line="1537"/>
        <location filename="mainwindow.cpp" line="1543"/>
        <location filename="mainwindow.cpp" line="1606"/>
        <location filename="mainwindow.cpp" line="1611"/>
        <location filename="mainwindow.cpp" line="1720"/>
        <source>ALL probe</source>
        <translation>프로브</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="152"/>
        <location filename="mainwindow.cpp" line="513"/>
        <location filename="mainwindow.cpp" line="1310"/>
        <location filename="mainwindow.cpp" line="1720"/>
        <source>probe1</source>
        <translation>프로브1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="266"/>
        <source>time</source>
        <translation>시간</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <source>name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="268"/>
        <location filename="mainwindow.cpp" line="1045"/>
        <location filename="mainwindow.cpp" line="1316"/>
        <location filename="mainwindow.cpp" line="1318"/>
        <location filename="mainwindow.cpp" line="1320"/>
        <location filename="mainwindow.cpp" line="1322"/>
        <location filename="mainwindow.cpp" line="1351"/>
        <location filename="mainwindow.cpp" line="1726"/>
        <location filename="mainwindow.cpp" line="1728"/>
        <location filename="mainwindow.cpp" line="1730"/>
        <location filename="mainwindow.cpp" line="1732"/>
        <source>machine_name</source>
        <translation>설비이름</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="269"/>
        <source>statue</source>
        <translation>상태</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="272"/>
        <location filename="mainwindow.cpp" line="519"/>
        <location filename="mainwindow.cpp" line="523"/>
        <location filename="mainwindow.cpp" line="543"/>
        <source>ALL</source>
        <translation>전체</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="336"/>
        <location filename="mainwindow.cpp" line="359"/>
        <location filename="mainwindow.cpp" line="382"/>
        <location filename="mainwindow.cpp" line="406"/>
        <location filename="mainwindow.cpp" line="430"/>
        <source>USCHDOWN3</source>
        <translation>설비고장</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="834"/>
        <source>PTrate</source>
        <translation>공정,설비 테스트</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="853"/>
        <source>PMrate</source>
        <translation>예방정비</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="872"/>
        <source>MTrate</source>
        <translation>설비고장</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="891"/>
        <source>Waitrate</source>
        <translation>대기</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="910"/>
        <location filename="mainwindow.cpp" line="1074"/>
        <location filename="mainwindow.cpp" line="1496"/>
        <location filename="mainwindow.cpp" line="1682"/>
        <source>USCHDOWN4</source>
        <translation>순간정지</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="928"/>
        <location filename="mainwindow.cpp" line="1076"/>
        <location filename="mainwindow.cpp" line="1498"/>
        <location filename="mainwindow.cpp" line="1685"/>
        <source>USCHDOWN1</source>
        <translation>자재품절</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="946"/>
        <location filename="mainwindow.cpp" line="1078"/>
        <location filename="mainwindow.cpp" line="1500"/>
        <location filename="mainwindow.cpp" line="1688"/>
        <source>SCHDOWN2</source>
        <translation>기종변경</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="964"/>
        <location filename="mainwindow.cpp" line="1080"/>
        <location filename="mainwindow.cpp" line="1502"/>
        <location filename="mainwindow.cpp" line="1691"/>
        <source>USCHDOWN2</source>
        <translation>품질문제</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="982"/>
        <location filename="mainwindow.cpp" line="1082"/>
        <location filename="mainwindow.cpp" line="1504"/>
        <location filename="mainwindow.cpp" line="1694"/>
        <source>NONSCHED</source>
        <translation>휴지</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1031"/>
        <location filename="mainwindow.cpp" line="1089"/>
        <location filename="mainwindow.cpp" line="1182"/>
        <location filename="mainwindow.cpp" line="1509"/>
        <location filename="mainwindow.cpp" line="1582"/>
        <source>ALLPRocess</source>
        <translation>전체</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1044"/>
        <location filename="mainwindow.cpp" line="1350"/>
        <source>process</source>
        <translation>공정</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1046"/>
        <location filename="mainwindow.cpp" line="1352"/>
        <source>stop_time</source>
        <translation>정지 시간</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1047"/>
        <location filename="mainwindow.cpp" line="1353"/>
        <source>run_time</source>
        <translation>가동 시간</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1048"/>
        <location filename="mainwindow.cpp" line="1354"/>
        <source>stop_data</source>
        <translation>정지 사유</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1049"/>
        <location filename="mainwindow.cpp" line="1355"/>
        <source>run_name</source>
        <translation>진행이름</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1050"/>
        <location filename="mainwindow.cpp" line="1356"/>
        <source>stop_name</source>
        <translation>정지이름</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1051"/>
        <location filename="mainwindow.cpp" line="1357"/>
        <source>stop_time_calc</source>
        <translation>정지로스</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1064"/>
        <location filename="mainwindow.cpp" line="1486"/>
        <location filename="mainwindow.cpp" line="1667"/>
        <source>run</source>
        <translation>정상가동</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1066"/>
        <location filename="mainwindow.cpp" line="1488"/>
        <location filename="mainwindow.cpp" line="1670"/>
        <source>PT</source>
        <translation>공정,설비테스트</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1068"/>
        <location filename="mainwindow.cpp" line="1490"/>
        <location filename="mainwindow.cpp" line="1673"/>
        <source>PM</source>
        <translation>예방정비</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1070"/>
        <location filename="mainwindow.cpp" line="1492"/>
        <location filename="mainwindow.cpp" line="1676"/>
        <source>MT</source>
        <translation>설비고장</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1072"/>
        <location filename="mainwindow.cpp" line="1494"/>
        <location filename="mainwindow.cpp" line="1679"/>
        <source>WAIT</source>
        <translation>대기</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1084"/>
        <source>name_order</source>
        <translation>이름순</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1220"/>
        <location filename="mainwindow.cpp" line="1620"/>
        <source>AVG</source>
        <translation>평균</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1317"/>
        <location filename="mainwindow.cpp" line="1319"/>
        <location filename="mainwindow.cpp" line="1321"/>
        <location filename="mainwindow.cpp" line="1323"/>
        <source>RUN</source>
        <translation>정상가동</translation>
    </message>
    <message>
        <source>operation_ratio</source>
        <translation type="vanished">가동률</translation>
    </message>
    <message>
        <source>stop_loss(%)</source>
        <translation type="vanished">정지로스(%)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1437"/>
        <source>restime</source>
        <translation>휴지시간</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1438"/>
        <source>removemin</source>
        <translation>제외(분)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1439"/>
        <source>content</source>
        <translation>내용</translation>
    </message>
</context>
<context>
    <name>Th_monitering</name>
    <message>
        <location filename="th_monitering.cpp" line="148"/>
        <location filename="th_monitering.cpp" line="150"/>
        <location filename="th_monitering.cpp" line="153"/>
        <location filename="th_monitering.cpp" line="158"/>
        <source>RUN</source>
        <translation>정상가동</translation>
    </message>
    <message>
        <source>SINMODE1</source>
        <translation type="vanished">SINMODE1</translation>
    </message>
    <message>
        <source>ONOMODE1</source>
        <translation type="vanished">ONOMODE1</translation>
    </message>
    <message>
        <location filename="th_monitering.cpp" line="155"/>
        <source>CVD_SiN</source>
        <translation type="unfinished">SINMODE1</translation>
    </message>
    <message>
        <location filename="th_monitering.cpp" line="160"/>
        <source>CVD_ONO</source>
        <translation type="unfinished">ONOMODE1</translation>
    </message>
    <message>
        <location filename="th_monitering.cpp" line="163"/>
        <location filename="th_monitering.cpp" line="165"/>
        <source>ENGR1</source>
        <translation>공정,설비 테스트</translation>
    </message>
    <message>
        <location filename="th_monitering.cpp" line="168"/>
        <location filename="th_monitering.cpp" line="170"/>
        <source>SCHDOWN1</source>
        <translation>예방정비</translation>
    </message>
    <message>
        <location filename="th_monitering.cpp" line="173"/>
        <location filename="th_monitering.cpp" line="174"/>
        <source>USCHDOWN3</source>
        <translation>설비고장</translation>
    </message>
    <message>
        <location filename="th_monitering.cpp" line="183"/>
        <location filename="th_monitering.cpp" line="185"/>
        <source>WAIT</source>
        <translation>대기</translation>
    </message>
    <message>
        <location filename="th_monitering.cpp" line="188"/>
        <location filename="th_monitering.cpp" line="190"/>
        <source>SCHDOWN2</source>
        <translation>기종변경</translation>
    </message>
    <message>
        <location filename="th_monitering.cpp" line="193"/>
        <location filename="th_monitering.cpp" line="195"/>
        <source>USCHDOWN4</source>
        <translation>순간정지</translation>
    </message>
    <message>
        <location filename="th_monitering.cpp" line="198"/>
        <location filename="th_monitering.cpp" line="200"/>
        <source>USCHDOWN1</source>
        <translation>자재품절</translation>
    </message>
    <message>
        <location filename="th_monitering.cpp" line="203"/>
        <location filename="th_monitering.cpp" line="205"/>
        <source>USCHDOWN2</source>
        <translation>품질문제</translation>
    </message>
    <message>
        <location filename="th_monitering.cpp" line="208"/>
        <location filename="th_monitering.cpp" line="210"/>
        <source>NONSCHED</source>
        <translation>휴지</translation>
    </message>
    <message>
        <location filename="th_monitering.cpp" line="238"/>
        <location filename="th_monitering.cpp" line="240"/>
        <source>dontknow</source>
        <translation>알수 없음</translation>
    </message>
</context>
<context>
    <name>add_machine_dialog</name>
    <message>
        <location filename="add_machine_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_machine_dialog.ui" line="59"/>
        <source>설비이름</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_machine_dialog.ui" line="67"/>
        <source>성막</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_machine_dialog.ui" line="72"/>
        <source>노광</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_machine_dialog.ui" line="77"/>
        <source>에칭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_machine_dialog.ui" line="82"/>
        <source>프로브</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_machine_dialog.ui" line="87"/>
        <source>프로브1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_machine_dialog.ui" line="98"/>
        <source>목록 번호</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_machine_dialog.ui" line="105"/>
        <source>공정</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_machine_dialog.ui" line="112"/>
        <source>설비코드</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="add_machine_dialog.ui" line="122"/>
        <source>MES설비 정보 -&gt; 설비 코드</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>join_popup</name>
    <message>
        <location filename="join_popup.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="join_popup.ui" line="22"/>
        <source>패스워드</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="join_popup.ui" line="29"/>
        <source>이름</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="join_popup.ui" line="36"/>
        <source>사번</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="join_popup.cpp" line="43"/>
        <source>have number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="join_popup.cpp" line="52"/>
        <source>join completet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>login_form</name>
    <message>
        <location filename="login_form.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login_form.ui" line="23"/>
        <source>이름</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login_form.ui" line="32"/>
        <source>로그인</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login_form.cpp" line="160"/>
        <source>db join false again try</source>
        <translation>DB에 접속을 실패 했습니다 다시 한번 시도해 주시기 바랍니다</translation>
    </message>
    <message>
        <location filename="login_form.cpp" line="175"/>
        <location filename="login_form.cpp" line="181"/>
        <location filename="login_form.cpp" line="208"/>
        <source>RUN</source>
        <translation>정상가동</translation>
    </message>
    <message>
        <source>SINMODE1</source>
        <translation type="vanished">SINMODE1</translation>
    </message>
    <message>
        <source>ONOMODE1</source>
        <translation type="vanished">ONOMODE1</translation>
    </message>
    <message>
        <location filename="login_form.cpp" line="183"/>
        <location filename="login_form.cpp" line="208"/>
        <source>CVD_SiN</source>
        <translation type="unfinished">SINMODE1</translation>
    </message>
    <message>
        <location filename="login_form.cpp" line="185"/>
        <location filename="login_form.cpp" line="208"/>
        <source>CVD_ONO</source>
        <translation type="unfinished">ONOMODE1</translation>
    </message>
    <message>
        <location filename="login_form.cpp" line="187"/>
        <source>ENGR1</source>
        <translation>공정,설비 테스트</translation>
    </message>
    <message>
        <location filename="login_form.cpp" line="189"/>
        <source>SCHDOWN1</source>
        <translation>예방정비</translation>
    </message>
    <message>
        <location filename="login_form.cpp" line="191"/>
        <source>USCHDOWN3</source>
        <translation>설비고장</translation>
    </message>
    <message>
        <location filename="login_form.cpp" line="193"/>
        <source>WAIT</source>
        <translation>대기</translation>
    </message>
    <message>
        <location filename="login_form.cpp" line="261"/>
        <source>ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>machine_list_item</name>
    <message>
        <location filename="machine_list_item.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="machine_list_item.ui" line="31"/>
        <source>설비 이름</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="machine_list_item.ui" line="49"/>
        <source>설비상태</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>manager_login</name>
    <message>
        <location filename="manager_login.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="manager_login.ui" line="20"/>
        <source>암호</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>modify_machine_dialog</name>
    <message>
        <location filename="modify_machine_dialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modify_machine_dialog.ui" line="22"/>
        <source>설비코드</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modify_machine_dialog.ui" line="29"/>
        <source>설비이름</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modify_machine_dialog.ui" line="39"/>
        <source>공정</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modify_machine_dialog.ui" line="47"/>
        <source>성막</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modify_machine_dialog.ui" line="52"/>
        <source>노광</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modify_machine_dialog.ui" line="57"/>
        <source>에칭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modify_machine_dialog.ui" line="62"/>
        <source>프로브</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modify_machine_dialog.ui" line="67"/>
        <source>프로브1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="modify_machine_dialog.ui" line="78"/>
        <source>목록번호</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>oi_select_ratio_view</name>
    <message>
        <location filename="oi_select_ratio_view.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_select_ratio_view.ui" line="32"/>
        <source>줌 초기화</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_select_ratio_view.cpp" line="50"/>
        <location filename="oi_select_ratio_view.cpp" line="128"/>
        <source>run</source>
        <translation type="unfinished">정상가동</translation>
    </message>
    <message>
        <location filename="oi_select_ratio_view.cpp" line="51"/>
        <location filename="oi_select_ratio_view.cpp" line="131"/>
        <source>PT</source>
        <translation type="unfinished">공정,설비테스트</translation>
    </message>
    <message>
        <location filename="oi_select_ratio_view.cpp" line="52"/>
        <location filename="oi_select_ratio_view.cpp" line="134"/>
        <source>PM</source>
        <translation type="unfinished">예방정비</translation>
    </message>
    <message>
        <location filename="oi_select_ratio_view.cpp" line="53"/>
        <location filename="oi_select_ratio_view.cpp" line="137"/>
        <source>MT</source>
        <translation type="unfinished">설비고장</translation>
    </message>
    <message>
        <source>WAIT</source>
        <translation type="obsolete">대기</translation>
    </message>
    <message>
        <location filename="oi_select_ratio_view.cpp" line="55"/>
        <location filename="oi_select_ratio_view.cpp" line="143"/>
        <source>USCHDOWN4</source>
        <translation type="unfinished">순간정지</translation>
    </message>
    <message>
        <location filename="oi_select_ratio_view.cpp" line="56"/>
        <location filename="oi_select_ratio_view.cpp" line="146"/>
        <source>USCHDOWN1</source>
        <translation type="unfinished">자재품절</translation>
    </message>
    <message>
        <location filename="oi_select_ratio_view.cpp" line="57"/>
        <location filename="oi_select_ratio_view.cpp" line="149"/>
        <source>SCHDOWN2</source>
        <translation type="unfinished">기종변경</translation>
    </message>
    <message>
        <location filename="oi_select_ratio_view.cpp" line="58"/>
        <location filename="oi_select_ratio_view.cpp" line="152"/>
        <source>USCHDOWN2</source>
        <translation type="unfinished">품질문제</translation>
    </message>
    <message>
        <location filename="oi_select_ratio_view.cpp" line="59"/>
        <location filename="oi_select_ratio_view.cpp" line="155"/>
        <source>NONSCHED</source>
        <translation type="unfinished">휴지</translation>
    </message>
</context>
<context>
    <name>oi_system_view1</name>
    <message>
        <location filename="oi_system_view1.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_system_view1.ui" line="203"/>
        <location filename="oi_system_view1.ui" line="448"/>
        <location filename="oi_system_view1.ui" line="604"/>
        <source>대기</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_system_view1.ui" line="257"/>
        <location filename="oi_system_view1.ui" line="376"/>
        <location filename="oi_system_view1.ui" line="520"/>
        <source>고장</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_system_view1.ui" line="275"/>
        <source>총</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_system_view1.ui" line="562"/>
        <source>TEST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_system_view1.ui" line="147"/>
        <source>Probe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_system_view1.ui" line="185"/>
        <location filename="oi_system_view1.ui" line="221"/>
        <location filename="oi_system_view1.ui" line="239"/>
        <location filename="oi_system_view1.ui" line="293"/>
        <location filename="oi_system_view1.ui" line="352"/>
        <location filename="oi_system_view1.ui" line="424"/>
        <location filename="oi_system_view1.ui" line="472"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_system_view1.ui" line="167"/>
        <location filename="oi_system_view1.ui" line="400"/>
        <location filename="oi_system_view1.ui" line="646"/>
        <source>P M</source>
        <oldsource>PM</oldsource>
        <translation type="unfinished">예방정비</translation>
    </message>
    <message>
        <source>60Ascr</source>
        <oldsource>60A스크러버</oldsource>
        <translation type="vanished">60스크러버</translation>
    </message>
    <message>
        <source>629scr</source>
        <oldsource>629스크러버</oldsource>
        <translation type="vanished">629스크러버</translation>
    </message>
    <message>
        <location filename="oi_system_view1.cpp" line="135"/>
        <location filename="oi_system_view1.cpp" line="144"/>
        <source>USCHDOWN3</source>
        <translation>설비고장</translation>
    </message>
    <message>
        <location filename="oi_system_view1.cpp" line="137"/>
        <location filename="oi_system_view1.cpp" line="146"/>
        <source>SCHDOWN1</source>
        <translation>예방정비</translation>
    </message>
    <message>
        <location filename="oi_system_view1.cpp" line="139"/>
        <location filename="oi_system_view1.cpp" line="148"/>
        <source>WAIT</source>
        <translation>대기</translation>
    </message>
    <message>
        <source>RUN</source>
        <translation type="vanished">정상가동</translation>
    </message>
    <message>
        <source>EA</source>
        <translation type="vanished">대</translation>
    </message>
    <message>
        <source>total</source>
        <translation type="vanished">총</translation>
    </message>
</context>
<context>
    <name>oi_total_ratio_view</name>
    <message>
        <location filename="oi_total_ratio_view.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.ui" line="44"/>
        <source>줌 초기화</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.ui" line="66"/>
        <source>순간정지</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.ui" line="73"/>
        <source>공정,설비테스트</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.ui" line="80"/>
        <source>설비고장</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.ui" line="87"/>
        <source>예방정비</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.ui" line="94"/>
        <source>기종변경</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.cpp" line="45"/>
        <location filename="oi_total_ratio_view.cpp" line="140"/>
        <location filename="oi_total_ratio_view.cpp" line="141"/>
        <source>run</source>
        <translation>정상가동</translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.cpp" line="46"/>
        <location filename="oi_total_ratio_view.cpp" line="144"/>
        <location filename="oi_total_ratio_view.cpp" line="145"/>
        <source>PT</source>
        <translation>공정,설비테스트</translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.cpp" line="47"/>
        <location filename="oi_total_ratio_view.cpp" line="148"/>
        <location filename="oi_total_ratio_view.cpp" line="149"/>
        <source>PM</source>
        <translation>예방정비</translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.cpp" line="48"/>
        <location filename="oi_total_ratio_view.cpp" line="152"/>
        <location filename="oi_total_ratio_view.cpp" line="153"/>
        <source>MT</source>
        <translation>설비고장</translation>
    </message>
    <message>
        <source>WAIT</source>
        <translation type="vanished">대기</translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.cpp" line="50"/>
        <location filename="oi_total_ratio_view.cpp" line="160"/>
        <location filename="oi_total_ratio_view.cpp" line="161"/>
        <source>USCHDOWN4</source>
        <translation>순간정지</translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.cpp" line="51"/>
        <location filename="oi_total_ratio_view.cpp" line="164"/>
        <location filename="oi_total_ratio_view.cpp" line="165"/>
        <source>USCHDOWN1</source>
        <translation>자재품절</translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.cpp" line="52"/>
        <location filename="oi_total_ratio_view.cpp" line="168"/>
        <location filename="oi_total_ratio_view.cpp" line="169"/>
        <source>SCHDOWN2</source>
        <translation>기종변경</translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.cpp" line="53"/>
        <location filename="oi_total_ratio_view.cpp" line="172"/>
        <location filename="oi_total_ratio_view.cpp" line="173"/>
        <source>USCHDOWN2</source>
        <translation>품질문제</translation>
    </message>
    <message>
        <location filename="oi_total_ratio_view.cpp" line="54"/>
        <location filename="oi_total_ratio_view.cpp" line="176"/>
        <location filename="oi_total_ratio_view.cpp" line="177"/>
        <source>NONSCHED</source>
        <translation>휴지</translation>
    </message>
</context>
<context>
    <name>operating_ratio_action</name>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="48"/>
        <source>설비 고장</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="69"/>
        <source>정상가동</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="90"/>
        <source>대기
(재공,인력부족)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="112"/>
        <source>예방 정비</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="133"/>
        <source>공정,설비 
테스트</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="155"/>
        <source>공정</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="174"/>
        <source>성막</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="179"/>
        <source>에칭</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="184"/>
        <source>노광</source>
        <translation></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="189"/>
        <source>프로브</source>
        <oldsource>ALL 프로브</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="203"/>
        <source>설비</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.ui" line="239"/>
        <source>설비 종류</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.cpp" line="36"/>
        <source>deposition</source>
        <translation>성막</translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.cpp" line="37"/>
        <source>light</source>
        <translation>노광</translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.cpp" line="38"/>
        <source>eatching</source>
        <translation>에칭</translation>
    </message>
    <message>
        <location filename="operatingratio/operating_ratio_action.cpp" line="39"/>
        <source>ALL probe</source>
        <translation>프로브</translation>
    </message>
</context>
<context>
    <name>rest_time_widget</name>
    <message>
        <location filename="rest_time_widget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="rest_time_widget.ui" line="20"/>
        <source>추가</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="rest_time_widget.ui" line="27"/>
        <source>제거</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
