#include "manager_login.h"
#include "ui_manager_login.h"

manager_login::manager_login(QSqlDatabase db,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::manager_login)
{
    ui->setupUi(this);
    my_mesdb  = db;

}

manager_login::~manager_login()
{
    delete ui;
}

void manager_login::on_buttonBox_accepted()
{
    QSqlQuery query(my_mesdb);
    query.exec("SELECT * FROM `FAB`.`OI_system_config`");
    QString pw;
    if(query.next()){
       pw = query.value("manager_pw").toString();
    }

    if(ui->LE_Passwrod->text()==pw){
        return;
    }else {
        this->reject();
    }
}
