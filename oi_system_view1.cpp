#include "oi_system_view1.h"
#include "ui_oi_system_view1.h"


oi_system_view1::oi_system_view1(QSqlDatabase my_mesdb, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::oi_system_view1)
{
    ui->setupUi(this);
    this->my_mesdb =my_mesdb;

    model1 = new QStandardItemModel(10,9);
    QSqlQuery query(my_mesdb);
    query.exec("select * from OI_system_machine_table_ver2 "
               "where view1oderby1 BETWEEN 1 AND 9 "
               "ORDER BY `view1oderby1` ASC, `view1oderby2` ASC");
    while(query.next())
    {
        BStandardItem *item = new BStandardItem(query.value("view1name").toString());
        item->setMachine_code(query.value("machine_code").toString());
        model1->setItem(query.value("view1oderby2").toInt()-1,query.value("view1oderby1").toInt()-1,
                        item);
        item->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        QFont textfont;
        textfont.setBold(true);
        textfont.setPointSize(42);
        item->setFont(textfont);
        itemlist.append(item);

    }
    query.exec("select * from OI_system_machine_table_ver2 "
               "where view1oderby1 = '0' ");
    while(query.next()){
        BStandardItem *item = new BStandardItem(query.value("view1name").toString());
        item->setMachine_code(query.value("machine_code").toString());
        probe_itemlist.append(item);

    }

    ui->T1->setModel(model1);
    summary_loop.setInterval(1000);
    summary_loop.start();
    connect(&summary_loop,SIGNAL(timeout()),this,SLOT(summary_loop_timeout()));


}

BStandardItem *oi_system_view1::findforitem(QVector<BStandardItem *> &itemlist, QString machine_code)
{
    for(int i=0;i<itemlist.count();i++){
        if(itemlist.at(i)->getMachine_code()==machine_code){
            return itemlist.at(i);
        }
    }
    return 0;
}
/*
QStandardItem *oi_system_view1::new_item(QString text)
{
    QStandardItem *item  = new QStandardItem(text);
    item->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
    QFont font2;
    font2.setFamily(QString::fromUtf8("\353\247\221\354\235\200 \352\263\240\353\224\225"));
    font2.setPointSize(48);
    font2.setBold(true);
    font2.setWeight(75);
    item->setFont(font2);
    item->setForeground(QBrush(QColor("#FFFFFF")));
    item->setBackground(QBrush(QColor(0,170,0)));
    return item;

}
QStandardItem *oi_system_view1::new_item(QString text,QColor color)
{
    QStandardItem *item  = new QStandardItem(text);
    item->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
    QFont font2;
    font2.setFamily(QString::fromUtf8("\353\247\221\354\235\200 \352\263\240\353\224\225"));
    font2.setPointSize(48);
    font2.setBold(true);
    font2.setWeight(75);
    item->setFont(font2);
    item->setForeground(QBrush(QColor("#FFFFFF")));
    item->setBackground(QBrush(color));
    return item;
}
*/
oi_system_view1::~oi_system_view1()
{
    delete ui;
}

void oi_system_view1::T1_slot(machine_statue_data data)
{
    BStandardItem *item = findforitem(itemlist,data.getMachine_code());
    if(item != 0 ){
         item->setBackground(data.getColor());
         item->setForeground(QColor(0xff,0xff,0xff));
         item->setMachine_statue(data.current_event);
    }else {
        item = findforitem(probe_itemlist,data.getMachine_code());
        if(item != 0){
            item->setMachine_statue(data.current_event);
        }
    }

}

void oi_system_view1::T2_slot(machine_statue_data data)
{

}

void oi_system_view1::summary_loop_timeout()
{
    //고장.
    int USCHDOWN3_count = 0;
    //PM.
    int SCHDOWN1_count = 0;
    //대기.
    int WAIT_count = 0;

    //프로브 고장.
    int P_USCHDOWN3_count = 0;
    //프로브 PM.
    int P_SCHDOWN1_count = 0;
    //프로브 대기.
    int P_WAIT_count = 0;
    //프로브 RUN.
    int P_RUN_count = 0;
    //프로브 총 count.
    int P_machine_count = probe_itemlist.count();

    for(int i=0;i<itemlist.count();i++){
        if(itemlist.at(i)->getMachine_statue() == tr("USCHDOWN3")){
            USCHDOWN3_count++;
        }else if(itemlist.at(i)->getMachine_statue() == tr("SCHDOWN1")){
            SCHDOWN1_count++;
        }else if(itemlist.at(i)->getMachine_statue() == tr("WAIT")){
            WAIT_count++;
        }
    }
    for(int i=0;i<probe_itemlist.count();i++){
        if(probe_itemlist.at(i)->getMachine_statue() == tr("USCHDOWN3")){
            P_USCHDOWN3_count++;
        }else if(probe_itemlist.at(i)->getMachine_statue() == tr("SCHDOWN1")){
            P_SCHDOWN1_count++;
        }else if(probe_itemlist.at(i)->getMachine_statue() == tr("WAIT")){
            P_WAIT_count++;
        }
    }
    ui->LA_M_uschdown3->setText(QString("%1").arg(USCHDOWN3_count));
    ui->LA_M_schdown1->setText(QString("%1").arg(SCHDOWN1_count));
    ui->LA_M_wait->setText(QString("%1").arg(WAIT_count));
    ui->LA_P_total->setText(QString("%1").arg(P_machine_count));
    ui->LA_P_uschdown3->setText(QString("%1").arg(P_USCHDOWN3_count));
    ui->LA_P_schdown1->setText(QString("%1").arg(P_SCHDOWN1_count));
    ui->LA_P_wait->setText(QString("%1").arg(P_WAIT_count));

}

void oi_system_view1::closeEvent(QCloseEvent *event)
{
    this->deleteLater();
}


