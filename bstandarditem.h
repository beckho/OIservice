#ifndef BSTANDARDITEM_H
#define BSTANDARDITEM_H

#include <QObject>
#include <QWidget>
#include <QStandardItem>

class BStandardItem : public QObject,public QStandardItem
{
    Q_OBJECT
public:
    BStandardItem();
    explicit BStandardItem(const QString &text);
    BStandardItem(const QIcon &icon, const QString &text);
    explicit BStandardItem(int rows, int columns = 1);
    QString tag;
    QString machine_name;
    QString machine_code;
    QString machine_statue;
    QString getTag() const;
    void setTag(const QString &value);

    QString getMachine_name() const;
    void setMachine_name(const QString &value);
    QString getMachine_code() const;
    void setMachine_code(const QString &value);
    QString getMachine_statue() const;
    void setMachine_statue(const QString &value);
};

#endif // BSTANDARDITEM_H
