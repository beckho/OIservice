#include "bstandarditem.h"

BStandardItem::BStandardItem()
{

}

BStandardItem::BStandardItem(const QString &text):QStandardItem(text)
{

}

BStandardItem::BStandardItem(const QIcon &icon, const QString &text):QStandardItem(icon,text)
{

}

BStandardItem::BStandardItem(int rows, int columns):QStandardItem(rows,columns)
{

}

QString BStandardItem::getMachine_statue() const
{
    return machine_statue;
}

void BStandardItem::setMachine_statue(const QString &value)
{
    machine_statue = value;
}

QString BStandardItem::getMachine_code() const
{
    return machine_code;
}

void BStandardItem::setMachine_code(const QString &value)
{
    machine_code = value;
}

QString BStandardItem::getMachine_name() const
{
    return machine_name;
}

void BStandardItem::setMachine_name(const QString &value)
{
    machine_name = value;
}


QString BStandardItem::getTag() const
{
    return tag;
}

void BStandardItem::setTag(const QString &value)
{
    tag = value;
}
