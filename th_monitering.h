#ifndef TH_MONITERING_H
#define TH_MONITERING_H

#include <QObject>
#include <QThread>
#include <machine_statue_data.h>
#include <QVector>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDateTime>
#include <QMutex>
#include <QHash>
#define DBID "EIS"
#define DBPW "wisolfab!"
class Th_monitering : public QThread
{
    Q_OBJECT
public:
    explicit Th_monitering(QVector<machine_statue_data> datalist);
    Th_monitering(QVector<machine_statue_data> datalist, QMutex *mu);
    QVector<machine_statue_data> datalist;
    QHash<QString,machine_statue_data> *statue_map;
    QMutex *mu;
    QSqlDatabase ms_mesdb;
    QSqlDatabase my_mesdb;
    void re_db_connect();
private:
    void run();

signals:
    void send_listdata(machine_statue_data data);
    void send_listdata_view(machine_statue_data data);
    void send_test_query(QString count);
    void send_log_text(QString text);

public slots:
};

#endif // TH_MONITERING_H
