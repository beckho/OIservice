#ifndef MANAGER_LOGIN_H
#define MANAGER_LOGIN_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>

namespace Ui {
class manager_login;
}

class manager_login : public QDialog
{
    Q_OBJECT

public:
    explicit manager_login(QSqlDatabase db,QWidget *parent = 0);
    QSqlDatabase my_mesdb;

    ~manager_login();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::manager_login *ui;
};

#endif // MANAGER_LOGIN_H
